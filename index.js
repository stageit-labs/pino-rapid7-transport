#! /usr/bin/env node
"use strict";
const Logger = require("r7insight_node");
const Parse = require("fast-json-parse");
const { Writable } = require("readable-stream");
const pump = require("pump");
const split = require("split2");

function r7insightOps(opts) {
    const splitter = split(function (line) {
        const parsed = new Parse(line);
        if (parsed.err) {
            this.emit("unknown", line, parsed.err);
            return undefined;
        }
        return parsed.value;
    });

    const logger = new Logger({ token: opts.token, region: "us" });

    const writable = new Writable({
        objectMode: true,
        highWaterMark: opts["bulk-size"] || 500,
        writev: function (chunks, cb) {
            switch (chunks.level) {
                case 30:
                    chunks.level = "INFO";
                    logger.info(chunks);
                    break;
                case 40:
                    chunks.level = "WARN";
                    logger.warning(chunks);
                    break;
                case 50:
                    chunks.level = "ERROR";
                    logger.err(chunks);
                    break;
                case 60:
                    chunks.level = "FATAL";
                    logger.crit(chunks);
                    break;
                default:
                    chunks.level = "DEBUG";
                    logger.debug(chunks);
            }
            cb();
        },
        write: function (chunk, enc, cb) {
            switch (chunk.level) {
                case 30:
                    chunk.level = "INFO";
                    logger.info(chunk);
                    break;
                case 40:
                    chunk.level = "WARN";
                    logger.warning(chunk);
                    break;
                case 50:
                    chunk.level = "ERROR";
                    logger.err(chunk);
                    break;
                case 60:
                    chunk.level = "FATAL";
                    logger.crit(chunk);
                    break;
                default:
                    chunk.level = "DEBUG";
                    logger.debug(chunk);
            }
            cb();
        },
    });

    pump(splitter, writable);
    return splitter;
}

module.exports = r7insightOps;
function start(opts) {
    pump(process.stdin, r7insightOps(opts));
}
if (require.main === module) {
    start({ token: process.argv[2] });
}
